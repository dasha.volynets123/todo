"use strict";

{
    class App {
        constructor(element) { //для создания экземпляра класса
            this.element = element; 
            this.titleInput = this.element.querySelector('#titleInput');
            this.descriptionInput = this.element.querySelector('#descriptionInput');
            this.importanceSelect = this.element.querySelector('#importanceSelect');
            this.cardsDataArray=[]; //массив с данными карточек
            //this.cardsArray=[];  // массив с сущностями карточек (их данные, элементы, кнопки)
            this.init(); //вызываем метод init, описанный ниже
        }

        attachEvents(){
            this.createButton.addEventListener('click', event => {
            event.preventDefault(); // preventDefault обнуляет действие по умолчанию (кнопка не будет отправлять форму на сервер)
        
            
            let cardData = {
                title: titleInput.value,
                description: descriptionInput.value,
                importance: importanceSelect.value
            }
            
            new Card(cardData);
            this.clearForm();

            
            let tempCardData = cardData;
            tempCardData.status = false; 

            this.cardsDataArray.push(tempCardData);
            this.updateStorage(tempCardData);

            
            });  

            this.editButton.addEventListener('click', event =>{
                this.editedCardData.title = this.titleInput.value;
                this.editedCardData.description = this.descriptionInput.value;
                this.editedCardData.importance = this.importanceSelect.value;

                this.createButton.style.display = "block";
                this.editButton.style.display = "none";
                this.clearForm();

                this.editedCard.updateCard();
                this.updateStorage();                
            })
        }

        updateStorage(){
            let stringifyCardArray = JSON.stringify(this.cardsDataArray);
            localStorage.setItem('todoCards',stringifyCardArray);
        }   

        clearForm() {
            this.titleInput.value="";
            this.descriptionInput.value="";
            this.importanceSelect.value="Low";
        }

        checkStorage(){
            let storageData = localStorage.getItem('todoCards');
            
            if (storageData) {
                this.cardsDataArray = JSON.parse(storageData);
                this.cardsDataArray.forEach(cardData => {
                   if(cardData){
                    new Card(cardData);
                   }                    
                });
            }
        }

        getAppElements(){
            this.createButton = this.element.querySelector('#createButton'); // находим кнопку Create через форму
            this.editButton = this.element.querySelector("#editButton");
        }

        updateCard(card){
            this.editedCard = card;
            this.editedCardData = card.cardData;
            this.fillForm(this.editedCardData);
            
            //console.log(this.editedCard); та карточка, на которой мы нажали Edit
            //console.log(this.cardsDataArray); //массив с данными карточек
            //console.log(this.cardsArray); все карточки с их данными и элементами (полная сущность)
            
        }        

        fillForm(cardData){
            this.titleInput.value=cardData.title;
            this.descriptionInput.value=cardData.description;
            this.importanceSelect.value=cardData.importance;

            this.createButton.style.display = "none";
            this.editButton.style.display = "block";
        }


       

    

        init() {
            this.getAppElements();
            this.attachEvents(); 
            this.checkStorage();           
        }

    }

    class Card {
        constructor(cardData) {
            this.cardData = cardData;
            this.cardsBlock=document.querySelector("#cardsBlock");
            this.card = document.createElement('div');
            this.init();
        }

       

        createCard(){
           
            this.card.classList.add('card');

            this.card.innerHTML = this.cardHTML;
            this.cardsBlock.append(this.card);

            console.log(this.cardData);
        }             

        get importanceClass(){
            switch (this.cardData.importance){
                case 'High':
                    return 'badge-danger'
                case 'Medium':
                    return  'badge-warning'
                default:
                    return  'badge-success'
            }
        }

        
        get completeClass(){
            return this.cardData.status ? 'btn-success' : 'btn-primary';
        }

        get completeText(){
            return this.cardData.status ? 'Completed' : 'Complete';
        }

        get completeStyles(){
            return this.cardData.status ? 'pointer-events: none; opacity=0.5': '';
        }

        getCardElements(){
            this.deleteButton = this.card.querySelector('.delete-button');
            this.editButton = this.card.querySelector('.edit-button');
            this.completeButton = this.card.querySelector('.complete-button');

        }

        editCard(){
            console.log(this);
            app.updateCard(this);
        }

        deleteCard(){
            let cardIndex = app.cardsDataArray.indexOf(this.cardData);
            delete app.cardsDataArray[cardIndex];

            let stringifyCardArray = JSON.stringify(app.cardsDataArray);
            localStorage.setItem('todoCards',stringifyCardArray);

            this.cardsBlock.removeChild(this.card);
        }

        updateCard(){
            let cardTitleNode = this.card.querySelector('.card-title');
            let cardDescriptionNode = this.card.querySelector('.card-description');
            let cardImportanceNode = this.card.querySelector('.badge');
            
            cardTitleNode.textContent = this.cardData.title;
            cardDescriptionNode.textContent = this.cardData.description;
            cardImportanceNode.textContent = this.cardData.importance;

            cardImportanceNode.className=`badge ${this.importanceClass}`;           
        }

        get cardHTML() {
            return `<div class="card-body">
                    <span class="badge ${this.importanceClass}">${this.cardData.importance}</span>  
                    <h5 class="card-title">${this.cardData.title}</h5>
                    <p class="card-description">${this.cardData.description}</p>
                    <a href="#" class="btn ${this.completeClass} complete-button" style=${this.completeStyles}>${this.completeText}</a>
                    <a href="#" class="btn btn-info edit-button" style=${this.completeStyles}>Edit</a>
                    <a href="#" class="btn btn-danger delete-button">Delete</a>
                </div>`;
        }

        completeCard(){
            this.cardData.status = true;
            app.updateStorage();
            this.completeButton.textContent="Completed";
            this.completeButton.style.pointerEvents='none';
            this.completeButton.style.opacity='0.5';
            this.completeButton.className = 'btn btn-success complete-button';
            this.editButton.style.pointerEvents='none';
            this.editButton.style.opacity='0.5';
            console.log(this.cardData); 
        }
        
        attachEvents(){
            let deleteCardWrapper = this.deleteCard.bind(this);
            this.deleteButton.addEventListener('click', deleteCardWrapper);

            let editCardWrapper = this.editCard.bind(this);
            this.editButton.addEventListener('click', editCardWrapper);

            let completeCardWrapper = this.completeCard.bind(this);
            this.completeButton.addEventListener('click', completeCardWrapper);
        }

        init(){
            this.createCard();
            this.getCardElements();
            this.attachEvents();
        }

        
    }

    let appElement = document.querySelector('#app'); // нашли всю форму, обернутую в див с id "app";

    let app = new App(appElement); // вызов конструктора по форме
}